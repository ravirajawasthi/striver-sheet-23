import java.util.*;

public class BoundaryTraversalGFG {

    public static void main(String[] args) {

        int[] tree = CheckIdenticalTrees.createArrayFromString("6 4 9 -1 10 4 5 -1 10 1 9 -1 8 6 1 -1 5 9 3 -1 6 8 7 -1 4 4");
        Node root = createABtFromArray(tree);
        System.out.println(boundary(root));
    }

    private static class Node {
        int data;
        Node left;
        Node right;

        Node(int data) {
            this.data = data;
            this.left = null;
            this.right = null;
        }

    }


    static ArrayList<Integer> boundary(Node node) {


        Deque<Node> stack = new LinkedList<>();

        //bottom view
        ArrayList<Integer> bottomTraversal = new ArrayList<>();

        if (node.right != null) stack.push(node.right);
        if (node.left != null) stack.push(node.left);


        while (!stack.isEmpty()) {
            Node root = stack.pop();

            if (root.left == null && root.right == null) {
                bottomTraversal.add(root.data);
                continue;
            }

            if (root.right != null) {
                stack.push(root.right);
            }
            if (root.left != null) {
                stack.push(root.left);
            }


        }

        Node curr = node.left;
        //Left view
        ArrayList<Integer> leftView = new ArrayList<>();
        leftView.add(node.data);
        while (curr != null) {
            if (curr.left != null || curr.right != null) {
                leftView.add(curr.data);
            }

            if (curr.left != null) {
                curr = curr.left;
            } else {
                curr = curr.right;
            }
        }


        //Right view
        ArrayList<Integer> rightView = new ArrayList<>();
        curr = node.right; //skipping node because It's covered in left view
        while (curr != null) {
            if (curr.left != null || curr.right != null) {
                rightView.add(curr.data);
            }
            if (curr.right != null) {
                curr = curr.right;
            } else {
                curr = curr.left;
            }
        }


        //creating ans
        leftView.addAll(bottomTraversal);

        for (int i = rightView.size() - 1; i > -1; i--) {
            leftView.add(rightView.get(i));
        }


        return leftView;
    }

    private static Node createABtFromArray(int[] elements) {
        int currIndex = 0;
        Node root = new Node(elements[currIndex++]);
        Queue<Node> queue = new ArrayDeque<>();
        queue.add(root);
        while (currIndex < elements.length) {
            Node bucket = queue.remove();
            if (elements[currIndex] != -1) {
                bucket.left = new Node(elements[currIndex++]);
                queue.add(bucket.left);
            } else {
                currIndex++;
            }
            if (currIndex < elements.length) {
                if (elements[currIndex] != -1) {
                    bucket.right = new Node(elements[currIndex++]);
                    queue.add(bucket.right);
                } else {
                    currIndex++;
                }
            }

        }
        return root;

    }


}
