import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class StackUsingQueuesTest {

    @Test
    void correctnessTest() {
        StackUsingQueues.Stack stack = new StackUsingQueues.Stack();

        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);

        assertEquals(stack.top(), 4);
        assertEquals(stack.pop(), 4);
        assertEquals(stack.top(), 3);
        assertEquals(stack.pop(), 3);
        assertEquals(stack.top(), 2);
        assertEquals(stack.pop(), 2);
        assertEquals(stack.top(), 1);
        assertEquals(stack.pop(), 1);
        assertEquals(stack.top(), -1);
        assertEquals(stack.pop(), -1);
    }
}