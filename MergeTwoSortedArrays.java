import java.util.Arrays;

public class MergeTwoSortedArrays {
    public static void main(String[] args) {
        System.out.println("Arrays.toString(ninjaAndSortedArrays(new int[]{1, 2, 3, 0, 0}, new int[]{4, 5}, 3, 2)) = " + Arrays.toString(ninjaAndSortedArrays(new int[]{1, 4, 6, 10, 0, 0}, new int[]{2, 5}, 4, 2)));
    }

    public static int[] ninjaAndSortedArrays(int arr1[], int arr2[], int m, int n) {
        int a1 = 0;
        int a2 = 0;
        while (a1 < (m + n) && a2 < n) {
            if (arr1[a1] == 0) {
                arr1[a1++] = arr2[a2++];
            } else if (arr1[a1] > arr2[a2]) {
                shiftArray(arr1, a1);
                arr1[a1++] = arr2[a2++];
            } else {
                a1++;
            }
        }
        return arr1;
    }

    private static void shiftArray(int[] arr, int index) {
        if (index == arr.length - 1) {
            return;
        }
        int prev = arr[index];
        for (int i = index + 1; i < arr.length; i++) {
            int bucket = arr[i];
            if (bucket == 0) {
                arr[i] = prev;
                break;
            }
            arr[i] = prev;
            prev = bucket;
        }

    }
}
