import java.util.ArrayDeque;
import java.util.Queue;

public class HeightBalancedBinaryTree {
    public static void main(String[] args) {
        int[] tree = CheckIdenticalTrees.createArrayFromString("1 2 3 4 -1 -1 -1 -1 5 -1 -1 ");
        BinaryTreeNode<Integer> root = createABtFromArray(tree);
        System.out.println("isBalancedBT(root) = " + isBalancedBT(root));

    }

    static class BinaryTreeNode<T> {
        T data;
        BinaryTreeNode<T> left;
        BinaryTreeNode<T> right;

        BinaryTreeNode(T data) {
            this.data = data;
            left = null;
            right = null;
        }
    }

    static private class Pair {
        public int height;
        public boolean isBalanced;

        public Pair(int height, boolean isBalanced) {
            this.height = height;
            this.isBalanced = isBalanced;
        }
    }

    public static boolean isBalancedBT(BinaryTreeNode<Integer> root) {
        return isBalanced(root).isBalanced;
    }

    private static Pair isBalanced(BinaryTreeNode<Integer> root) {
        Pair left;
        Pair right;
        if (root.left == null) {
            left = new Pair(0, true);
        } else {
            left = isBalanced(root.left);
        }

        if (root.right == null) {
            right = new Pair(0, true);
        } else {
            right = isBalanced(root.right);
        }


        if (!left.isBalanced || !right.isBalanced) {
            return new Pair(0, false);
        }

        if (Math.abs(left.height - right.height) > 1) {
            return new Pair(0, false);
        } else {
            return new Pair(Math.max(left.height, right.height) + 1, true);
        }

    }

    private static BinaryTreeNode<Integer> createABtFromArray(int[] elements) {
        int currIndex = 0;
        BinaryTreeNode<Integer> root = new BinaryTreeNode<>(elements[currIndex++]);
        Queue<BinaryTreeNode<Integer>> queue = new ArrayDeque<>();
        queue.add(root);
        while (currIndex < elements.length) {
            BinaryTreeNode<Integer> bucket = queue.remove();
            if (elements[currIndex] != -1) {
                bucket.left = new BinaryTreeNode<>(elements[currIndex++]);
                queue.add(bucket.left);
            } else {
                currIndex++;
            }
            if (currIndex < elements.length) {
                if (elements[currIndex] != -1) {
                    bucket.right = new BinaryTreeNode<>(elements[currIndex++]);
                    queue.add(bucket.right);
                } else {
                    currIndex++;
                }
            }

        }
        return root;

    }

}
