//https://www.codingninjas.com/codestudio/problems/maximum-subarray-sum_8230694?challengeSlug=striver-sde-challenge
public class MaxSubArray {
    public static void main(String[] args) {
        System.out.println("maxSubarraySum(new int[]{-1}, 1) = " + maxSubarraySum(new int[]{-21, 1, -3}, 2));
    }

    //TODO code works in leetcode, doesnt work on coding ninjas?
    //for leetcode currSum will be Integer.MIN_VALUE
    public static long maxSubarraySum(int[] arr, int n) {
        int currSum = 0;
        int num;
        int ans = currSum;
        for (int i = 0; i < n; i++) {
            num = arr[i];
            currSum += num;
            ans = Math.max(currSum, ans);
            if (currSum < 0) {
                currSum = 0;
            }
        }

        return ans;

    }


}
