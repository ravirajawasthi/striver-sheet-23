import java.util.*;

public class BinaryTreeZigZagTraversal {
    public static void main(String[] args) {
        BinaryTreeNode root = createABtFromArray(new int[]{1, 2, 3, -1, -1, -1, 6, -1, -1});
        System.out.println("zigZagTraversal(root) = " + zigZagTraversal(root));
    }


    static class BinaryTreeNode<T> {
        T data;
        BinaryTreeNode<T> left;
        BinaryTreeNode<T> right;

        public BinaryTreeNode(T data) {
            this.data = data;
        }
    }

    public static List<Integer> zigZagTraversal(BinaryTreeNode<Integer> root) {
        List<Integer> ans = new LinkedList<>();
        Queue<BinaryTreeNode<Integer>> queue = new LinkedList<>();
        boolean leftToRight = true;
        queue.add(root);
        while (!queue.isEmpty()) {
            BinaryTreeNode<Integer> element = queue.remove();
            int size = queue.size();
            List<Integer> tempStorage = new LinkedList<>();
            tempStorage.add(element.data);

            if (element.left != null) {
                queue.add(element.left);
            }
            if (element.right != null) {
                queue.add(element.right);
            }

            for (int i = 0; i < size; i++) {
                BinaryTreeNode<Integer> node = queue.remove();
                tempStorage.add(node.data);

                if (node.left != null) {
                    queue.add(node.left);
                }
                if (node.right != null) {
                    queue.add(node.right);
                }

            }
            if (!leftToRight) {
                Collections.reverse(tempStorage);
            }
            ans.addAll(tempStorage);

            leftToRight = !leftToRight;
        }
        return ans;

    }

    private static BinaryTreeNode createABtFromArray(int[] elements) {
        int currIndex = 0;
        BinaryTreeNode root = new BinaryTreeNode<>(elements[currIndex++]);
        Queue<BinaryTreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        while (currIndex < elements.length) {
            BinaryTreeNode bucket = queue.remove();
            if (elements[currIndex] != -1) {
                bucket.left = new BinaryTreeNode<>(elements[currIndex++]);
                queue.add(bucket.left);
            } else {
                currIndex++;
            }
            if (currIndex < elements.length) {
                if (elements[currIndex] != -1) {
                    bucket.right = new BinaryTreeNode<>(elements[currIndex++]);
                    queue.add(bucket.right);
                } else {
                    currIndex++;
                }
            }

        }
        return root;

    }
}
