public class Sort012 {
    public static void main(String[] args) {
        sort012(new int[]{0, 1, 2, 2, 1, 0});
    }

    public static void sort012(int[] arr) {
        int[] count = new int[]{0, 0, 0};

        for (int n : arr) {
            count[n]++;
        }
        for (int z = 0; z < count[0]; z++) {
            System.out.print("0 ");
        }
        for (int z = 0; z < count[1]; z++) {
            System.out.print("1 ");
        }
        for (int z = 0; z < count[2]; z++) {
            System.out.print("2 ");
        }

    }

}
