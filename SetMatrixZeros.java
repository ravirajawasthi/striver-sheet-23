
//https://www.codingninjas.com/codestudio/problems/set-matrix-zeros_8230862?challengeSlug=striver-sde-challenge

import java.util.Arrays;

class SetMatrixZeros {

    public static void main(String[] args) {
        int[][] matrix = new int[][] {
                { 7, 19, 3 },
                { 4, 21, 0 }
        };
        setZeros(matrix);
        System.out.println(Arrays.deepToString(matrix));
    }

    public static void setZeros(int[][] matrix) {
        int rows = matrix.length;
        int columns = matrix[0].length;

        boolean firstRowEmpty = false;
        boolean firstColumnEmpty = false;
        for (int i = 0; i < columns; i++) {
            if (matrix[0][i] == 0) {
                firstRowEmpty = true;
                break;
            }
        }

        for (int i = 0; i < rows; i++) {
            if (matrix[i][0] == 0) {
                firstColumnEmpty = true;
                break;
            }
        }

        for (int i = 1; i < rows; i++) {
            for (int j = 1; j < columns; j++) {
                if (matrix[i][j] == 0) {
                    matrix[0][j] = 0;
                    matrix[i][0] = 0;
                }
            }
        }

        // Emptying the rows first marked to be zeroed
        for (int i = 1; i < rows; i++) {
            if (matrix[i][0] == 0) {
                matrix[i] = Arrays.copyOf(new int[] { 0 }, columns);
            }
        }

        // Emptying the columns marked to be zeroed
        for (int j = 0; j < columns; j++) {
            if (matrix[0][j] == 0) {
                for (int i = 0; i < rows; i++) {
                    matrix[i][j] = 0;
                }
            }
        }

        if (firstColumnEmpty) {
            for (int i = 0; i < rows; i++) {
                matrix[i][0] = 0;
            }
        }

        if (firstRowEmpty) {
            matrix[0] = Arrays.copyOf(new int[] { 0 }, columns);
        }

    }
}