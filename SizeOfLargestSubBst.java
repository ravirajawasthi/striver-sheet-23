import java.util.ArrayDeque;
import java.util.Queue;

public class SizeOfLargestSubBst {
    public static void main(String[] args) {
        int[] tree = CheckIdenticalTrees.createArrayFromString("50 -1 20 -1 30 -1 40 -1 50 -1 -1");
        TreeNode<Integer> root = createABtFromArray(tree);
        System.out.println("largestBST(root) = " + largestBST(root));
    }

    static private class TreeNode<T> {
        public T data;
        public TreeNode<T> left;
        public TreeNode<T> right;

        TreeNode(T data) {
            this.data = data;
            left = null;
            right = null;
        }
    }

    private static class RecursiveReturn {
        public int height;
        public int smallest;
        public int largest;
        public boolean isBst;

        public RecursiveReturn(int height) {
            this.height = height;
            this.smallest = Integer.MAX_VALUE;
            this.largest = Integer.MIN_VALUE;
            this.isBst = false;
        }

        public RecursiveReturn(int height, int smallest, int largest, boolean isBst) {
            this.height = height;
            this.smallest = smallest;
            this.largest = largest;
            this.isBst = isBst;
        }
    }


    public static int largestBST(TreeNode<Integer> root) {

        return maxBstHeight(root).height;
    }

    private static RecursiveReturn maxBstHeight(TreeNode<Integer> root) {
        if (root.left == null && root.right == null) {
            return new RecursiveReturn(1, root.data, root.data, true);
        }

        RecursiveReturn left = root.left == null ? new RecursiveReturn(0, 0, 0, true) : maxBstHeight(root.left);
        RecursiveReturn right = root.right == null ? new RecursiveReturn(0, 0, 0, true) : maxBstHeight(root.right);

        //Checking if this subtree is bst
        if (!left.isBst || !right.isBst || root.right != null && root.data > right.smallest || root.left != null && root.data < left.largest) {
            int currMaxHeight = Math.max(left.height, right.height);
            return new RecursiveReturn(currMaxHeight, 0, 0, false);
        }

        int max = root.right == null ? root.data : right.largest;
        int min = root.left == null ? root.data : left.smallest;
        int height = left.height + right.height + 1;

        return new RecursiveReturn(height, min, max, true);


    }


    private static TreeNode<Integer> createABtFromArray(int[] elements) {
        int currIndex = 0;
        TreeNode<Integer> root = new TreeNode<>(elements[currIndex++]);
        Queue<TreeNode<Integer>> queue = new ArrayDeque<>();
        queue.add(root);
        while (currIndex < elements.length) {
            TreeNode<Integer> bucket = queue.remove();
            if (elements[currIndex] != -1) {
                bucket.left = new TreeNode<>(elements[currIndex++]);
                queue.add(bucket.left);
            } else {
                currIndex++;
            }
            if (currIndex < elements.length) {
                if (elements[currIndex] != -1) {
                    bucket.right = new TreeNode<>(elements[currIndex++]);
                    queue.add(bucket.right);
                } else {
                    currIndex++;
                }
            }

        }
        return root;

    }

}
