import java.util.*;
import java.util.stream.Collectors;

class NextSmallerElement {
    public static void main(String[] args) {
        System.out.println(nextSmallerElement(Arrays.stream(new int[]{2, 1, 4, 3}).boxed().collect(Collectors.toCollection(ArrayList::new)), 4));
    }

    private static class Pair {
        public Integer num;
        public Integer index;

        Pair(Integer num, Integer index) {
            this.num = num;
            this.index = index;
        }
    }

    static ArrayList<Integer> nextSmallerElement(ArrayList<Integer> arr, int n) {
        Stack<Pair> stack = new Stack<>();
        ArrayList<Integer> ans = new ArrayList<>(Arrays.asList(new Integer[n]));
        for (int i = 0; i < n; i++) {
            int num = arr.get(i);
            if (stack.isEmpty() || stack.peek().num <= num) {
                stack.push(new Pair(num, i));
            } else {
                while (!stack.isEmpty() && stack.peek().num > num) {
                    ans.set(stack.pop().index, num);
                }
                stack.push(new Pair(num, i));
            }
        }
        while (!stack.isEmpty()) {
            Pair element = stack.pop();
            ans.set(element.index, -1);
        }
        return ans;

    }
}
