import java.util.Stack;

public class QueueUsingStack {


    public class Queue {

        Stack<Integer> s1 = new Stack<>();
        Stack<Integer> s2 = new Stack<>();

        Queue() {

        }

        void enQueue(int val) {
            s1.push(val);
        }

        int deQueue() {
            if (s1.isEmpty()) {
                return -1;
            }
            while (!s1.isEmpty()) {
                s2.push(s1.pop());
            }
            int returnItem = s2.pop();
            while (!s2.isEmpty()) {
                s1.push(s2.pop());
            }
            return returnItem;
        }

        int peek() {
            if (s1.isEmpty()) {
                return -1;
            }
            while (!s1.isEmpty()) {
                s2.push(s1.pop());
            }
            int returnItem = s2.peek();
            while (!s2.isEmpty()) {
                s1.push(s2.pop());
            }
            return returnItem;
        }

        boolean isEmpty() {
            return s1.isEmpty();
        }
    }
}
