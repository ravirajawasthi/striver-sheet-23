import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class StackUsingQueues {
    public static void main(String[] args) throws FileNotFoundException {

    }
    public static class Stack {

        Queue<Integer> q1;
        Queue<Integer> q2;


        public Stack() {
            q1 = new LinkedList<>();
            q2 = new LinkedList<>();
        }



        /*----------------- Public Functions of Stack -----------------*/


        public int getSize() {
            return q1.size();
        }

        public boolean isEmpty() {
            return q1.isEmpty();
        }

        public void push(int element) {
            q1.add(element);
        }

        public int pop() {
            if (q1.size() == 0){
                return -1;
            }
            int size = q1.size();
            for (int i = 0; i < size - 1; i++) {
                q2.add(q1.remove());
            }
            size = q2.size();
            int returnItem = q1.remove();
            for (int i = 0; i < size; i++) {
                q1.add(q2.remove());
            }
            return returnItem;
        }

        public int top() {
            if (q1.size() == 0){
                return -1;
            }
            int size = q1.size();
            for (int i = 0; i < size - 1; i++) {
                q2.add(q1.remove());
            }
            int returnItem = q1.peek();
            q2.add(q1.remove());
            size = q2.size();
            for (int i = 0; i < size; i++) {
                q1.add(q2.remove());
            }
            return returnItem;
        }
    }
}
