import java.util.ArrayList;


class PascalTriangle {
    public static void main(String[] args) {
        System.out.println(printPascal(5));

    }

    public static ArrayList<ArrayList<Long>> printPascal(int n) {
        ArrayList<ArrayList<Long>> triangle = new ArrayList<>();
        ArrayList<Long> level1 = new ArrayList<>();
        level1.add(1L);
        triangle.add(new ArrayList<>(level1));

        ArrayList<Long> newLevel;

        for (int level = 2; level <= n; level++) {

            newLevel = new ArrayList<>(level);

            ArrayList<Long> previousLevel = triangle.get(triangle.size() - 1);
            newLevel.add(1L);
            for (int i = 1; i < level - 1; i++) {
                newLevel.add(previousLevel.get(i) + previousLevel.get(i - 1));
            }
            newLevel.add(1L);
            triangle.add(newLevel);
        }

        return triangle;
    }

}
