import java.util.ArrayList;
import java.util.Arrays;

public class RepeatingAndMissing {
    public static void main(String[] args) {
        ArrayList<Integer> nums = new ArrayList<>();
//        nums.add(6);
//        nums.add(5);
//        nums.add(4);
//        nums.add(2);
//        nums.add(2);
//        nums.add(1);
        nums.add(3);
        nums.add(3);
        nums.add(2);
        nums.add(1);
        System.out.println("Arrays.toString(missingAndRepeating(nums, 6)) = " + Arrays.toString(missingAndRepeating(nums, 4)));

    }

    public static int[] missingAndRepeating(ArrayList<Integer> arr, int n) {
        int xor = arr.get(0);
        for (int i = 1; i < arr.size(); i++) {
            xor = xor ^ arr.get(i);
        }
        for (int i = 1; i <= n; i++) {
            xor = xor ^ i;
        }

        int bucket = 1;
        while ((bucket & xor) == 0) {
            bucket = bucket << 1;
        }

        int sameBit = 0;
        int diffBit = 0;

        for (int num : arr) {
            if ((num & bucket) == 0) {
                diffBit = diffBit ^ num;
            } else {
                sameBit = sameBit ^ num;
            }
        }

        for (int i = 1; i <= n; i++) {
            if ((i & bucket) == 0) {
                diffBit = diffBit ^ (i);
            } else {
                sameBit = sameBit ^ (i);
            }
        }


        for (int num : arr) {
            if (num == diffBit) {
                return new int[]{diffBit, sameBit};
            } else if (num == sameBit) {
                return new int[]{sameBit, diffBit};
            }

        }
        return new int[]{diffBit, sameBit};
    }
}
