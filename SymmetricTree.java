import java.util.*;

public class SymmetricTree {
    public static void main(String[] args) {
        int[] tree = CheckIdenticalTrees.createArrayFromString("1 2 2 3 4 4 3 -1 -1 -1 -1 -1 -1 -1 -1");
        BinaryTreeNode<Integer> root = createABtFromArray(tree);
        System.out.println(isSymmetric(root));
    }

    static class BinaryTreeNode<T> {
        T data;
        BinaryTreeNode<T> left;
        BinaryTreeNode<T> right;

        public BinaryTreeNode(T data) {
            this.data = data;
        }
    }

    public static boolean isSymmetric(BinaryTreeNode<Integer> root) {
        if ((root.left == null && root.right != null) || root.right == null && root.left != null) {
            return false;
        }

        Deque<BinaryTreeNode<Integer>> leftQueue = new LinkedList<>();
        Deque<BinaryTreeNode<Integer>> rightQueue = new LinkedList<>();

        leftQueue.add(root.left);
        rightQueue.add(root.right);

        while (!leftQueue.isEmpty()) {
            if (rightQueue.isEmpty()) {
                return false;
            }

            BinaryTreeNode<Integer> left = leftQueue.remove();
            BinaryTreeNode<Integer> right = rightQueue.remove();

            if (!Objects.equals(left.data, right.data)) {
                return false;
            }

            if (left.left != null) {
                leftQueue.add(left.left);
            }
            if (left.right != null) {
                leftQueue.add(left.right);
            }


            if (right.right != null) {
                rightQueue.add(right.right);
            }
            if (right.left != null) {
                rightQueue.add(right.left);
            }


        }
        return rightQueue.isEmpty();

    }

    private static BinaryTreeNode createABtFromArray(int[] elements) {
        int currIndex = 0;
        BinaryTreeNode root = new BinaryTreeNode(elements[currIndex++]);
        Queue<BinaryTreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        while (currIndex < elements.length) {
            BinaryTreeNode bucket = queue.remove();
            if (elements[currIndex] != -1) {
                bucket.left = new BinaryTreeNode(elements[currIndex++]);
                queue.add(bucket.left);
            } else {
                currIndex++;
            }
            if (currIndex < elements.length) {
                if (elements[currIndex] != -1) {
                    bucket.right = new BinaryTreeNode(elements[currIndex++]);
                    queue.add(bucket.right);
                } else {
                    currIndex++;
                }
            }

        }
        return root;
    }

}
