import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

//https://www.codingninjas.com/codestudio/problems/next-permutation_8230741?challengeSlug=striver-sde-challenge
//https://www.youtube.com/watch?v=JDOXKqF60RQ
public class NextPermutation {
    public static void main(String[] args) {
        System.out.println("nextPermutation(new ArrayList<>(Arrays.asList(9, 8, 6, 7, 5, 4, 3, 2, 1))) = " + nextPermutation(new ArrayList<>(Arrays.asList(1, 5, 3, 2, 4))));
    }

    public static ArrayList<Integer> nextPermutation(ArrayList<Integer> permutation) {
        int size = permutation.size();
        int breakPointIndex = -1; //Where we find the first element that is smaller when traversing the array from end
        for (int i = size - 1; i > 0; i--) {
            if (permutation.get(i - 1) < permutation.get(i)) {
                breakPointIndex = i - 1;
                break;
            }
        }
        if (breakPointIndex == -1) {
            Collections.reverse(permutation);
            return permutation;
        }
        Integer breakPoint = permutation.get(breakPointIndex);
        int bestDelta = Integer.MAX_VALUE;
        int swappingCandidate = -1;
        int delta;
        //The smallest value that we can swap. that value will be before the breaking point only
        //TODO Can we optimise the approach to find the best element to swap?
        for (int i = size - 1; i > breakPointIndex; i--) {
            Integer num = permutation.get(i);
            if (num > breakPoint) {
                delta = num - breakPoint;
                if (delta < bestDelta) {
                    bestDelta = delta;
                    swappingCandidate = i;
                }
            }
        }
        int bucket = permutation.get(swappingCandidate);
        permutation.set(swappingCandidate, breakPoint);
        permutation.set(breakPointIndex, bucket);

        //reversing now
        int startIndex = breakPointIndex + 1;
        int endIndex = size - 1;
        while (endIndex > startIndex) {

            int temp = permutation.get(startIndex);
            permutation.set(startIndex, permutation.get(endIndex));
            permutation.set(endIndex, temp);

            startIndex++;
            endIndex--;
        }
        return permutation;
    }


}
