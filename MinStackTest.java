import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MinStackTest {

    @Test
    public void correctnessTest(){
        MinStack stack = new MinStack();

        stack.push(1);
        stack.push(2);

        assertEquals(stack.getMin(), 1);
        assertEquals(stack.pop(), 2);
        assertEquals(stack.top(), 1);

    }

}