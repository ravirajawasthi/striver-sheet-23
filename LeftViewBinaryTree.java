import java.util.*;

public class LeftViewBinaryTree {

    public static void main(String[] args) {
        TreeNode root = createABtFromArray(new int[]{3, 4, -1, -1, -1});
        System.out.println(getLeftView(root));

    }

    static class TreeNode<T> {
        T data;
        TreeNode<T> left;
        TreeNode<T> right;

        TreeNode(T data) {
            this.data = data;
            left = null;
            right = null;
        }
    }


    static class Pair<L, R> {

        Pair(L left, R right) {
            this.node = left;
            this.delta = right;
        }

        public L node;
        public R delta;
    }

    public static ArrayList<Integer> getLeftView(TreeNode<Integer> root) {
        if (root == null) {
            return new ArrayList<>();
        }
        Queue<TreeNode<Integer>> queue = new LinkedList<>();
        ArrayList<Integer> bottomView = new ArrayList<>();

        queue.add(root);

        while (!queue.isEmpty()) {
            TreeNode<Integer> element = queue.remove();
            int currLevelSize = queue.size();
            bottomView.add(element.data);
            if (element.left != null) queue.add(element.left);
            if (element.right != null) queue.add(element.right);
            for (int i = 0; i < currLevelSize; i++) {
                element = queue.remove();
                if (element.left != null) queue.add(element.left);
                if (element.right != null) queue.add(element.right);
            }
        }

        return bottomView;

    }

    private static TreeNode<Integer> createABtFromArray(int[] elements) {
        int currIndex = 0;
        TreeNode<Integer> root = new TreeNode<>(elements[currIndex++]);
        Queue<TreeNode<Integer>> queue = new ArrayDeque<>();
        queue.add(root);
        while (currIndex < elements.length) {
            TreeNode<Integer> bucket = queue.remove();
            if (elements[currIndex] != -1) {
                bucket.left = new TreeNode<>(elements[currIndex++]);
                queue.add(bucket.left);
            } else {
                currIndex++;
            }
            if (currIndex < elements.length) {
                if (elements[currIndex] != -1) {
                    bucket.right = new TreeNode<>(elements[currIndex++]);
                    queue.add(bucket.right);
                } else {
                    currIndex++;
                }
            }

        }
        return root;

    }
}
