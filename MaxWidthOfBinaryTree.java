import java.util.LinkedList;
import java.util.Queue;

public class MaxWidthOfBinaryTree {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }
    }

    public static int getMaxWidth(TreeNode root) {
        if (root == null) {
            return -1;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        int ans = 0;

        while (!queue.isEmpty()) {

            int size = queue.size();
            ans = Math.max(ans, size);

            for (int i = 0; i < size; i++) {

                TreeNode element = queue.remove();

                if (element.left != null) {
                    queue.add(element.left);
                }

                if (element.right != null) {
                    queue.add(element.right);
                }
            }


        }
        return ans;
    }
}
