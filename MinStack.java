
class MinStack {

    private static class Pair {
        public int data;
        private final int min;

        Pair(int data, int min) {
            this.data = data;
            this.min = min;
        }
    }

    private static class ListNode<T> {
        ListNode<T> next;
        T data;

        ListNode(T data) {
            this.next = null;
            this.data = data;
        }

        ListNode(T data, ListNode<T> next) {
            this.next = next;
            this.data = data;
        }
    }

    private ListNode<Pair> head;

    // Constructor
    MinStack() {
        head = null;
    }

    // Function to add another element equal to num at the top of stack.
    void push(int num) {
        if (head == null) {
            head = new ListNode<>(new Pair(num, num));
        } else {
            int newMin = Math.min(head.data.min, num);
            head = new ListNode<>(new Pair(num, newMin), head);
        }
    }

    // Function to remove the top element of the stack.
    int pop() {
        if (head == null) {
            return -1;
        } else {
            int data = head.data.data;
            head = head.next;
            return data;
        }
    }

    // Function to return the top element of stack if it is present. Otherwise
    // return -1.
    int top() {
        if (head == null) {
            return -1;
        }
        return head.data.data;
    }

    // Function to return minimum element of stack if it is present. Otherwise
    // return -1.
    int getMin() {
        if (head == null) {
            return -1;
        }
        return head.data.min;
    }

}

