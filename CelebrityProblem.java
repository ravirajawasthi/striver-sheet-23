import java.util.Stack;

public class CelebrityProblem {

    public static int findCelebrity(int n) {
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < n; i++) {
            stack.push(i);
        }

        while (stack.size() != 1) {
            int one = stack.pop();
            int two = stack.pop();

            if (Runner.knows(one, two)) {
                stack.push(two);
            } else {
                stack.push(one);
            }
        }

        int candidate = stack.pop();

        for (int i = 0; i < n; i++) {
            if (i == candidate) {
                continue;
            }

            if (!(!Runner.knows(candidate, i) && Runner.knows(i, candidate))) {
                return -1;
            }

        }
        return candidate;

    }

    private static class Runner {
        //Implemented by coding ninja ide
        public static boolean knows(int i, int j) {
            return true;
        }
    }
}
